o ""
echo "*********************************************" 
echo "*                                           *" 
echo "* LAMP AIO Docker Image (Alpine Linux core) *" 
echo "*                                           *" 
echo "*   Docker Image         (Build 20180216)   *"
echo "*                                           *"
echo "*********************************************"
echo ""
echo "Starting the Docker Image, Please Wait ..."
echo ""
# execute any pre-init scripts
echo -e "\033[33m -> Running pre-init scripts ...\033[0m" 
for i in /scripts/pre-init.d/*sh
do
	if [ -e "${i}" ]; then
		echo " --> pre-init.d - processing $i"
		. "${i}"
	fi
done
echo -e "\033[33m -> Checking MySQL socket ...\033[0m"
if [ -d "/run/mysqld" ]; then
	echo " --> /run/mysqld already present !"
    echo " --> Changing privilege for /run/mysqld ..."
	chown -R mysql:mysql /run/mysqld
else
	echo " --> /run/mysqld not found, creating...."
	mkdir -p /run/mysqld
    echo " --> Changing privilege for /run/mysqld ..."
	chown -R mysql:mysql /run/mysqld
fi

echo -e "\033[33m -> Checking MySQL directory ...\033[0m" 
if [ -d /var/lib/mysql/mysql ]; then
	echo " --> MySQL directory already present !"
    echo " --> Changing privilege for /var/lib/mysql ..."
	chown -R mysql:mysql /var/lib/mysql
else
    echo " --> MySQL data directory not found !"
    echo " --> Changing privilege for /var/lib/mysql ..."
	chown -R mysql:mysql /var/lib/mysql
    echo " --> MySQL data directory not found, creating initial database ..."
    echo " --> Please wait, this may take a while ..."
	mysql_install_db --user=mysql >/dev/null 2>&1
	echo -e " --> Checking MySQL Root Password ..." 
	if [ "$MYSQL_ROOT_PASSWORD" = "" ]; then
        echo " --> Not finding any paramters about MySQL Password !"
        echo " --> Generating MySQL Root Password ..."
		MYSQL_ROOT_PASSWORD=`pwgen 16 1`
        echo " --> Generate Success !"
		echo " --> MySQL root Password is : $MYSQL_ROOT_PASSWORD"
	fi

	MYSQL_DATABASE=${MYSQL_DATABASE:-""}
	MYSQL_USER=${MYSQL_USER:-""}
	MYSQL_PASSWORD=${MYSQL_PASSWORD:-""}

	tfile=`mktemp`
	if [ ! -f "$tfile" ]; then
	    return 1
	fi

echo -e "\033[33m -> Initializing MySQL Database ...\033[0m" 

	cat << EOF > $tfile
USE mysql;
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' identified by '$MYSQL_ROOT_PASSWORD' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' identified by '$MYSQL_ROOT_PASSWORD' WITH GRANT OPTION;
UPDATE user SET password=PASSWORD("$MYSQL_ROOT_PASSWORD") WHERE user='root' AND host='localhost';
EOF

	if [ "$MYSQL_DATABASE" != "" ]; then
	    echo " --> Creating database: $MYSQL_DATABASE"
	    echo "CREATE DATABASE IF NOT EXISTS \`$MYSQL_DATABASE\` CHARACTER SET utf8 COLLATE utf8_general_ci;" >> $tfile
	    if [ "$MYSQL_USER" != "" ]; then
		echo " --> Creating user: $MYSQL_USER with password $MYSQL_PASSWORD"
		echo "grant all privileges on $MYSQL_DATABASE.* to $MYSQL_USER@localhost identified by '$MYSQL_PASSWORD';" >> $tfile
		echo "FLUSH PRIVILEGES;" >> $tfile
	    fi
	fi
	echo -e "\033[33m --> Initializing MySQL ...\033[0m" 
	/usr/bin/mysqld --user=mysql --bootstrap --verbose=0 < $tfile
	rm -f $tfile
fi

# execute any pre-exec scripts
echo -e "\033[33m -> Running pre-exec scripts ...\033[0m" 
for i in /scripts/pre-exec.d/*sh
do
	if [ -e "${i}" ]; then
		echo " --> pre-exec.d - processing $i"
		. ${i}
	fi
done

echo -e "\033[33m -> Starting MariaDB (MySQL) in deamon mode ...\033[0m"
exec nohup /usr/bin/mysqld_safe --user=mysql >/dev/null 2>&1 &
echo ""
echo " >>> Information of the running enviroment <<<"
echo ""
echo " Linux Version : Alpine Linux $(cat /etc/alpine-release)"
echo " Linux Kernel Version : $(uname -r)"
echo " Linux Architecture : $(uname -m)"
echo " Docker Hostname : $(uname -n)"
echo " Server IP : $(curl -s whatismyip.akamai.com)"
echo ""
echo -e " Linux ROOT User Password : \033[41;37m $ROOT_PASSWORD \033[0m"
echo -e " MySQL ROOT User Password : \033[41;37m $MYSQL_ROOT_PASSWORD \033[0m"
echo -e " MySQL Database Name : \033[41;37m $MYSQL_DATABASE \033[0m"
echo -e " MySQL Username : \033[41;37m $MYSQL_USER \033[0m"
echo -e " MySQL Password : \033[41;37m $MYSQL_PASSWORD \033[0m"
echo ""
echo "***********************************************"
echo ""
echo -e "\033[33m Starting Apache ...\033[0m" ;\
echo ""
echo -e "\033[33m Start Success ! Enjoy your docker LAMP enviroment ! \033[0m"
echo ""
exec httpd -D FOREGROUND
