## LAMP AIO Image (LAMP一键部署镜像Docker版)  
  
### 0. 镜像说明  
  
 > 此镜像目前仍然处于测试阶段（也就是传说中的概念版），发现BUG或者有更好的解决方案，欢迎Email到：ilemonrain@ilemonrain.com，谢谢大家的使用与帮助！  

### 1. 镜像Tag  
  > **```ilemonrain/lamp```**, **```ilemonrain/lamp:alpine```** ：基于Alpine Linux核心的LAMP镜像

### 2. 镜像使用说明
 > **强烈推荐使用“镜像构建教程”中的方法，手动构建自己需要的Docker镜像！此镜像仅仅是体验！**  
 > 
 > **docker run -t -p 8800:80 -e MYSQL_DATABASE="testdb" -e MYSQL_USER="testuser" -e MYSQL_PASSWORD="testpassword" -e MYSQL_ROOT_PASSWORD="rootpass" ilemonrain/lamp:alpine**    
 > * -t：显示启动过程，启动成果后可用Ctrl+C关闭到后台运行；（亦可以用-d替换，直接进入后台运行，但务必至少等待30秒后，即镜像展开部署完成后再行访问！）
 > * -p 8800:80：端口映射，容器内部Apache端口默认为80，外部端口请按实际需要进行映射；
 > * -e MYSQL_DATABASE：指定启动时建立的数据库名称；
 > * -e MYSQL_USER：指定启动时建立的用户名；
 > * -e MYSQL_PASSWORD：指定启动时设定的MYSQL_USER的密码；
 > * -e MYSQL_ROOT_PASSWORD：指定MySQL数据库的ROOT账户密码 

### 3. 镜像构建教程
 > 首先，用Git工具，clone一份项目到本地：
 > ```
 > git clone https://ilemonrain@bitbucket.org/ilemonrain/docker-ilemonrain-lnmp.git
 > ```
 > 随后进入目录，你会发现这样的目录结构：
 > ```
 > .  
 > └── lnmp-alpine  
 >  ├── Dockerfile  
 >  ├── entrypoint.sh  
 >  ├── scripts  
 >  │   ├── pre-exec.d  
 >  │   │   └── default.sh  
 >  │   └── pre-init.d  
 >  │       └── default.sh  
 >  └── wwwroot  
 >      └── index.html  
 > ```
 > 接下来说明下各个文件/目录的功能：  
 > * Dockerfile：构建项目用的工程文件，稍后会讲到如何修改；  
 > * entrypoint.sh：程序启动时运行的默认程序，非特殊情况一般不用动这个文件；  
 > * scripts/pre-init.d：在镜像启动后，开始展开部署前，需要运行的程序，请放到这里；
 > * scripts/pre-exec.d：在部署完成后，启动环境前，需要运行的程序，请放到这里；
 > * wwwroot：网站文件目录请放到这里
 >  
 > 如果不需要额外添加其他组件的话，请直接把你的网站文件放入wwwroot目录中，然后进行构建：
 > ```
 > docker build -t="my-lnmp-image" .
 > ```
 > 注意命令后面的英文句号（点），很多人因为没看到那个点，死活构建不出来，一定要注意！  
 >  
 > 在构建结束后（提示Successfully Built <镜像ID>）后，请执行命令：
 > ```
 > docker run -t -p 8800:80 my-lnmp-image
 > ```
 > 之后会自动进行展开部署，请耐心等待，直到出现（All Success ! Enjoy your docker LAMP enviroment !）时，请访问你的服务器IP地址（如果映射了端口号，记得带上端口号），即可开始使用镜像！
 >   
 > 如果缺少了PHP模块组件，那么请用文本编辑器（推荐使用VS Code，并安装Docker扩展），打开Dockerfile文件，找到如下段落：
 > ```
 > echo -e "\033[33m Installing PHP 7 ...\033[0m" ;\  
 > apk add php7-cli php7-json php7-phar php7-openssl php7-zlib php7-bcmath \  
 > php7-dba php7-enchant php7-gd php7-intl php7-json php7-mbstring \  
 > php7-mysqlnd php7-opcache php7-pdo php7-pdo_mysql php7-soap \  
 >  php7-ctype php7-session ;\  
 > ```
 > 加上你需要的组件即可；比如需要加入php redis扩展，那么填上php7-redis即可（Alpine Linux中一般都是在扩展名前加“**php7-**”前缀）
 >   
 > Apache自定义httpd.conf功能我会在以后的更新加进去。
 >  
 > 如果需要修改启动时创建的数据库，或者控制编译时使用的Alpine Linux软件包镜像源，除了可以通过上面的“镜像使用说明”中的启动命令，也可以修改Dockerfile中对应的代码，实现修改默认镜像启动时的动作：
 > ```
 > ENV ROOT_PASSWORD="alpine" \  
 >     APK_MIRROR="dl-cdn.alpinelinux.org" \  
 >     APK_MIRROR_SCHEME="http" \  
 >     MYSQL_DATABASE="testdb" \  
 >     MYSQL_USER="testuser" \  
 >     MYSQL_PASSWORD="testpassword" \  
 >     MYSQL_ROOT_PASSWORD="rootpass"  
 > ```
 > * ROOT_PASSWORD：Alpine Linux的系统ROOT账户密码，以后在加入SSH功能后会有搭配使用；
 > * APK_MIRROR(实验性)：修改构建镜像时使用的镜像源，加快构建速度；
 > * APK_MIRROR_SCHEME(实验性)：指定镜像源使用http协议或是https协议；
 > * MYSQL_DATABASE：指定启动时建立的数据库名称；
 > * MYSQL_USER：指定启动时建立的用户名；
 > * MYSQL_PASSWORD：指定启动时设定的MYSQL_USER的密码；
 > * MYSQL_ROOT_PASSWORD：指定MySQL数据库的ROOT账户密码
