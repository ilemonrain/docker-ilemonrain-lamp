#!/bin/sh
echo "Installing ${WEBSITE_INSTALL_NAME} ..."
echo "Downloading from ${WEBSITE_INSTALL_URL} ..."
curl -sO ${WEBSITE_INSTALL_URL}
echo "Decompressing Package ..."
ls | tar xf $(grep tar.gz)
echo "Installing Package ..."
mv build/* ./
echo "Cleaning up ..."
rm -rf build/
rm -f *.tar.gz
echo "Install success !"
